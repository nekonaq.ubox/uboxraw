SHELL			= /bin/bash
app_name		= uboxraw

MAKE_RECURSE		= $(MAKE) --no-print-directory
PACKER_BUILD		= packer build -color=false

# build_box(output_name,template,var_files,vars)
define build_box =
( set -x; \
  $(PACKER_BUILD) \
    $(foreach item,$(3),-var-file $(item)) \
    -var output_name=$(1) \
    $(foreach item,$(4),-var $(item)) \
    $(2) \
  ; \
)
endef

#//
all:
	@$(MAKE_RECURSE) uboxraw/ubox.ovf

.PHONY: all

uboxraw/ubox.ovf: %.ovf:
	@$(call build_box,$(@D),$(or $(wildcard $(@D).json),uboxbase/$(@D).json),, \
	   vm_name=$(@D) \
	   output_directory=$(@D) \
	)
